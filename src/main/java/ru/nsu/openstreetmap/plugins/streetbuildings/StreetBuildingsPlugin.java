package ru.nsu.openstreetmap.plugins.streetbuildings;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;

public class StreetBuildingsPlugin extends Plugin {

    /**
     * Will be invoked by JOSM to bootstrap the plugin
     *
     * @param info  information about the plugin and its local installation
     */
    public StreetBuildingsPlugin(PluginInformation info) {
        super(info);
        System.out.println("*************************STREETBUILDINGS PLUGIN RUN********************");
    }
}
